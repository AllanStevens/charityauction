#Charity Auction Site#

This application was originally built in Visual Studio 2003 framework 1.1.  Latest source code has been upgraded to Visual Studio 2013 using framework 4.0.  

![auction.png](https://bitbucket.org/repo/dpxxxg/images/2747480243-auction.png)

Database is Microsoft SQL Server 2008, but would work fine with earlier versions if required.  

Requires windows authentication to be enabled and anonymous access to be disabled in IIS.  This is so that it will log the bidders username and possibly full name via Active Directory lookup.

The application works similar to that well known auction website.  However one slight difference is that you can not bid up other bidders.  For example if the auction is on £10 and you bid £100, the item will raise to £100.   I do plan to enhance the code so that it will raise to £20 (depending on the step interval) and you will remain the highest bidder until £110 is bid.  Feel free to make this change if it's required, all I ask is that you share your code here.

No download is supplied, all required code and database script/backups are in the Source Code section.

It would be great to hear from anyone that uses the application and raises money for charity.  It's raised thousands so far!

![item.png](https://bitbucket.org/repo/dpxxxg/images/3478489303-item.png)